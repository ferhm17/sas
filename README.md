﻿
- What is the average temperature for each city and how does the temperature vary?

- What is the average temperature for each year and how does the temperature vary?

- What is the average temperature for each year and city and how does the temperature vary?

- How are the temperatures distributed in each century?

- What is the frequency of the observations according to temperature range, century and city?

- What is del coldest and hotest year?

- What is the coldest and hotest city?

- How is the temperature trend over the years?

We have a dataset with average temperatures of many countries over the years and we are going to answer these 
questions for a specific country that is chosen at the begining. In first place, we are goint to filter the 
obsertions made in the chosen country and remove the observations with miss value. Then, we are going to apply 
different tools to answer the above questions.